# Copyright 2011 Paul Seidler
# Distributed under the terms of the GNU General Public License v2

require freedesktop-mime gnome.org [ suffix=tar.xz ] gsettings
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="Library for OpenPGP prompts"

LICENCES=""
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="gobject-introspection gtk-doc libnotify
    ( linguas: bn da en_GB bn_IN gl it kn mr ne pa ar ca@valencia et fi fr ja nb nn rw sv az bg ca de
              el en_CA eo gu he mk pl pt si sl sr dz en@shaw ga hi ko mai ms ru br eu id ml or ro te
              zh_CN zh_TW as cy es hr hu ku nl oc pt_BR sk sr@latin th ast be@latin cs lt lv ta ug
              uk vi sq tr zh_HK )

"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.35.0]
        sys-devel/gettext
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.9] )
    build+run:
        app-crypt/gnupg[>=2.0]
        app-crypt/gpgme[>=1.0]
        dev-libs/dbus-glib:1[>=0.35]
        dev-libs/glib:2[>=2.32.0] [[ note = [ For gsettings ] ]]
        gnome-desktop/gcr
        x11-libs/gtk+:3[>=3.0.0][gobject-introspection?]
        x11-libs/libICE
        x11-libs/libSM
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.4] )
        libnotify? ( x11-libs/libnotify[>=0.3] )
        !gnome-desktop/seahorse[<3.2]  [[ description = [ File collision ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/0001-daemon-port-to-gcr-3.patch )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    "--prefix=/usr"
    "--exec_prefix=/usr/$(exhost --target)"
    "--includedir=/usr/$(exhost --target)/include"
    "--disable-update-mime-database"
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gobject-introspection introspection' 'gtk-doc' 'libnotify' )

src_prepare() {
    # 2.2 is now supported as well
    # https://bugzilla.gnome.org/show_bug.cgi?id=790152
    edo sed -e 's/accepted_versions="1.2 1.4 2.0/& 2.1 2.2/' \
            -e '/AC_PATH_PROG(PKG_CONFIG/d' \
            -i configure.ac
    autotools_src_prepare
}

pkg_postrm() {
    freedesktop-mime_pkg_postrm
    gsettings_pkg_postrm
}

pkg_postinst() {
    freedesktop-mime_pkg_postinst
    gsettings_pkg_postinst
}

