# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ]
require meson
require python [ blacklist=none ]
require test-dbus-daemon

SUMMARY="Python Bindings for GObject"

UPSTREAM_DOCUMENTATION="https://pygobject.readthedocs.io"
LICENCES="LGPL-2.1"
SLOT="3"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="cairo"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.38.0]
        dev-libs/libffi[>=3.0]
        gnome-desktop/gobject-introspection:1[>=1.46.0]
        cairo? ( dev-python/pycairo[>=1.11.1][python_abis:*(-)?] )
        !gnome-bindings/pygobject:2[gobject-introspection] [[
            description = [ file collision ]
        ]]
        !gnome-bindings/pygobject:2[<2.28.6-r3] [[
            description = [ Versions before 2.28.6-r2 might install introspection-based bindings ]
            resolution = manual
        ]]
    test:
        x11-libs/gdk-pixbuf:2.0[gobject-introspection]
        x11-libs/gtk+:3[gobject-introspection]
        x11-libs/pango[gobject-introspection]
        cairo? ( x11-libs/cairo )
"

MESON_SRC_CONFIGURE_OPTION_SWITCHES=( 'cairo pycairo' )

configure_one_multibuild() {
    meson_src_configure -Dpython=${PYTHON}
}

compile_one_multibuild() {
    python_disable_pyc
    meson_src_compile
}

# Need access to wayland/X
RESTRICT="test"

test_one_multibuild() {
    unset DISPLAY
    unset GDK_IS_DISPLAY

    test-dbus-daemon_run-tests
}

install_one_multibuild() {
    meson_src_install
    python_bytecompile
}

